<?php

/******************************/
/* EurHisFirm custom settings */
/******************************/
$wgSitename = "EurHisFirm-Wikibase";

# Disable account creation and anonymous edits
# https://stuff.coffeecode.net/2018/wikibase-workshop-swib18.html#_disabling_account_creation_and_anonymous_edits
$wgGroupPermissions['*']['edit'] = false;
$wgGroupPermissions['*']['createaccount'] = false;

# Eurhisfirm Logo
$wgLogo = "$wgResourceBasePath/assets/logo_wb.png";

# Footer with image
# Docs: https://www.mediawiki.org/wiki/Manual:$wgFooterIcons
$wgFooterIcons = [
    "copyright" => [
        "copyright" => [], // placeholder for the built in copyright icon
    ],
    "poweredby" => [
        "mediawiki" => [
            "src" => "$wgResourceBasePath/assets/footer-ce.png",
            "height" => "75",
            "width" => "600",
		],
	],
];


/*******************************/
/* Enable Federated properties */
/*******************************/
#$wgWBRepoSettings['federatedPropertiesEnabled'] = true;

/*******************************/
/* Enables ConfirmEdit Captcha */
/*******************************/
#wfLoadExtension( 'ConfirmEdit/QuestyCaptcha' );
#$wgCaptchaQuestions = [
#  'What animal' => 'dog',
#];

#$wgCaptchaTriggers['edit']          = true;
#$wgCaptchaTriggers['create']        = true;
#$wgCaptchaTriggers['createtalk']    = true;
#$wgCaptchaTriggers['addurl']        = true;
#$wgCaptchaTriggers['createaccount'] = true;
#$wgCaptchaTriggers['badlogin']      = true;

/*******************************/
/* Disable UI error-reporting  */
/*******************************/
#ini_set( 'display_errors', 0 );
