# Eurhisfirm Wikibase Docker

This repository contains customizations made on top of the [Wikibase release pipeline](https://github.com/wmde/wikibase-release-pipeline/) to meet Eurhisfirm requirements.

Here it is important to stick to the design of the upstream Docker-Compose stack, to ease spotting the differences, and minimize actions required to upgrade.
In particular, it should be preferred to customize the environment variables (via the `.env` file) and the configuration files (e.g. `LocalSettings.php`) that are mounted as Docker volumes, over the Docker-Compose YAML files themselves.

See also: <https://www.mediawiki.org/wiki/Wikibase/Docker>

## List of customizations

Customizations done in `template.env`:

- Enable uploads

Note: when modifying `template.env`, ensure to deploy changes in the corresponding `.env` file in production.

Customizations done in `LocalSettings.php`:

- MediaWiki site name
- Eurhisfirm logo
- Footer in MediaWiki website
- Disable account creation and anonymous edits

## Configuration

The Docker-Compose stack needs environment variables to run.
Those variables are defined in a `.env` file that is automatically loaded by Docker-Compose.

Copy the template then change the secret values (i.e. passwords):

```bash
cp template.env .env

# Edit the .env file
```

## Deployment

The first time the Docker-Compose stack is run, or after any change in the configuration files, the Docker-Compose stack must be restarted:

```bash
docker-compose up -d

# To force a restart:
docker-compose restart
```

### Upgrade to new upstream version

When a new version of Wikibase is released, you can follow [this guide](https://github.com/wmde/wikibase-release-pipeline/blob/main/docs/topics/upgrading.md) to update this instance.

### Volumes backup

To backup all the Docker volumes at once before upgrading, and restore them afterward, you can use the following tools:

- [backup_volumes.sh](backup_volumes.sh): dump local volumes
- [restore_backup.sh](restore_backup.sh): restore volumes backup (to be used on remote server)
- [reinit.sh](reinit.sh): reset local volumes

### Extensions

When updating you'll also have to check if the used extensions have been added to the bundled extensions.
In that case it's not useful to explicitly include them anymore.

The list is [here](https://github.com/wmde/wikibase-release-pipeline/blob/main/Docker/build/WikibaseBundle/README.md#bundled-extensions).

## Local development

```bash
cd docker
cp template.env .env

# Edit the .env file to replace WIKIBASE_PORT=80 with another port (e.g. 8181).

docker-compose up
```

Then navigate to <http://localhost:8181>

The default username and password for MediaWiki are defined in the `.env` file.
